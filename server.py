import tornado.ioloop as ioloop
import tornado.web
import csv
from tornado.escape import json_decode, json_encode
from datetime import datetime
from json import load


class PreLoginHandler(tornado.web.RequestHandler):

    def initialize(self, user_info):
        self.user_info = user_info

    def post(self):
        self.set_header("Content-Type", "application/json")
        user_login = json_decode(self.request.body)
        try:
            user_search = next(item for item in self.user_info if item["username"] == user_login["username"])
            if user_search["password"] == user_login["password"]:
                self.set_status(200)
                self.write(
                    json_encode({"Name": user_search["Name"], "Surname": user_search["Surname"], "isAdmin": True,
                                 "minimizationTime": user_search["minimizationTime"]}))
            else:
                self.set_status(401)
                self.write("Unauthorized.")
        except StopIteration:
            self.set_status(404)
            self.write("Not found.")


class WarningLoginHandler(tornado.web.RequestHandler):

    def initialize(self, user_info):
        self.user_info = user_info

    def post(self):
        self.set_header("Content-Type", "application/json")
        user_login = json_decode(self.request.body)
        try:
            user_search = next(item for item in self.user_info if item["username"] == user_login["username"])
            if user_search["password"] == user_login["password"]:
                self.set_status(200)
                self.write(
                    json_encode({"Name": user_search["Name"], "Surname": user_search["Surname"], "isAdmin": True,
                                 "minimizationTime": user_search["minimizationTime"], "whlist": getting_hw("wh"),
                                 "token": "ADMIN_TOKEN", "lplist": getting_hw("lp")}))
            else:
                self.set_status(401)
                self.write("Unauthorized.")
        except StopIteration:
            self.set_status(404)
            self.write("Not found.")


class ManLoginHandler(tornado.web.RequestHandler):

    def initialize(self, user_info):
        self.user_info = user_info
        self.man_reservations = read_man_reservation()[0]

    def post(self):
        self.set_header("Content-Type", "application/json")
        user_login = json_decode(self.request.body)
        if self.man_reservations["id"] == user_login["reservation"]:
            if datetime.fromisoformat(self.man_reservations["endTime"]) > datetime.now():
                try:

                    user_search = next(
                        item for item in self.user_info["Admin"] if item["username"] == user_login["username"])
                    if user_search["password"] == user_login["password"]:
                        self.set_status(200)
                        self.write(
                            json_encode(
                                {"Name": user_search["Name"], "Surname": user_search["Surname"], "isAdmin": True}))
                    else:
                        self.set_status(401)
                        self.write("Unauthorized.")
                except StopIteration:
                    try:
                        user_search = next(
                            item for item in self.user_info["User"] if item["username"] == user_login["username"])
                        if user_search["password"] == user_login["password"]:
                            self.set_status(200)
                            self.write(
                                json_encode(
                                    {"Name": user_search["Name"], "Surname": user_search["Surname"], "isAdmin": False,
                                     "token": "AABBCC"}))
                        else:
                            self.set_status(401)
                            self.write("Unauthorized.")
                    except StopIteration:
                        self.set_status(404)
                        self.write("Not found.")
            else:
                self.set_status(400)
                self.write("This reservation expired at {}".format(self.man_reservations["endTime"]))
        else:
            self.set_status(404)
            self.write("No reservation found, maybe you logged in when the reservation expired.")


class AutoLoginHandler(tornado.web.RequestHandler):

    def initialize(self, user_info):
        self.user_info = user_info
        self.auto_reservations = read_auto_reservation()[0]

    def post(self):
        self.set_header("Content-Type", "application/json")
        user_login = json_decode(self.request.body)
        if self.auto_reservations["id"] == user_login["reservation"]:
            if datetime.fromisoformat(self.auto_reservations["endTime"]) > datetime.now():
                try:
                    user_search = next(
                        item for item in self.user_info["Admin"] if item["username"] == user_login["username"])
                    if user_search["password"] == user_login["password"]:
                        self.set_status(200)
                        self.write(
                            json_encode(
                                {"Name": user_search["Name"], "Surname": user_search["Surname"], "isAdmin": True,
                                 "minimizationTime": user_search["minimizationTime"]}))
                    else:
                        self.set_status(401)
                        self.write("Unauthorized.")
                except StopIteration:
                    try:
                        user_search = next(
                            item for item in self.user_info["User"] if item["username"] == user_login["username"])
                        if user_search["password"] == user_login["password"]:
                            self.set_status(200)
                            self.write(
                                json_encode(
                                    {"Name": user_search["Name"], "Surname": user_search["Surname"], "isAdmin": False,
                                     "token": "AABBCC",
                                     "minimizationTime": user_search["minimizationTime"]}))
                        else:
                            self.set_status(401)
                            self.write("Unauthorized.")
                    except StopIteration:
                        self.set_status(404)
                        self.write("Not found.")
            else:
                self.set_status(400)
                self.write("This reservation expired at {}".format(self.auto_reservations["endTime"]))
        else:
            self.set_status(404)
            self.write("No reservation found, maybe you logged in when the reservation expired.")


class ECUListHandler(tornado.web.RequestHandler):

    def get(self):
        self.set_header("Content-Type", "application/json")
        ecu_list_ver_proj_act = json_encode(load(open("hardware.json")))
        self.write(ecu_list_ver_proj_act)


class HWCheckHandler(tornado.web.RequestHandler):

    def post(self):
        self.set_header("Content-Type", "application/json")
        hw_linked = json_decode(self.request.body)
        hw_local = getting_hw(hw_linked["Type"])
        response = hw_check(hw_linked["Hw"], hw_local)
        if response == "Not Found":
            self.set_status(404)
        else:
            self.set_status(200)
        self.write(json_encode(response))


class EventHandler(tornado.web.RequestHandler):

    def post(self):
        events = json_decode(self.request.body)
        if events["hardware"] == "wh":
            logging("Wiring Harness Disconnected.")
        elif events["hardware"] == "lp":
            logging("Load Plate Disconnected.")
        else:
            logging("Wiring Harness and Load Plate Disconnected.")


class ConfigHandler(tornado.web.RequestHandler):

    def get(self):
        info = self.get_argument('info')
        self.set_header("Content-Type", "application/json")
        info_to_send = load(open("config.json"))
        self.write(json_encode(info_to_send[info]))


class LogoutHandler(tornado.web.RequestHandler):

    def delete(self):
        del_res = self.get_argument('id')
        res = read_man_reservation()
        if str(res[0]["id"]) == str(del_res):
            event = "Reservation with id {} is stopped and user {} has logged out.".format(str(del_res), res[0]["name"])
            logging(event)
            self.set_status(200)
        else:
            self.set_status(404)


class ReplaceHandler(tornado.web.RequestHandler):

    def post(self):
        token = self.request.headers.get("Authentication")
        print(token)
        hw = dict(json_decode(self.request.body))
        if "wh" in hw and "lp" in hw:
            logging("WH {} and LP {} are replaced with force replace.".format(hw["wh"], hw["lp"]))
            self.set_status(200)
        else:
            if "wh" in hw:
                logging("WH {} is replaced with force replace.".format(hw["wh"]))
                self.set_status(200)
            elif "lp" in hw:
                logging("LP {} is replaced with force replace.".format(hw["lp"]))
                self.set_status(200)
            else:
                self.set_status(400)


class ExpirationHandler(tornado.web.RequestHandler):

    def post(self):
        info = json_decode(self.request.body)
        logging(
            "User {} has a reservation with id {}, it will be notified to use the HIL before the definitive expiration.".format(
                info["name"], info["reservation"]))
        self.set_status(200)


class ManEditHandler(tornado.web.RequestHandler):

    def post(self):
        edited = dict(json_decode(self.request.body))
        event = "Changed some info for new access of {}: Software: {}, Version: {}, Project: {}, Activity: {}, Gate: {}.".format(
            edited["name"],
            edited["sw"],
            edited["version"],
            edited["project"],
            edited["activity"],
            edited["gate"]) if edited["gate"] != "" else \
            "Changed some info for new access of {}: Software: {}, Version: {}, Project: {}, Activity: {}. Gate not required".format(
                edited["name"],
                edited["sw"],
                edited["version"],
                edited["project"],
                edited["activity"])
        logging(event)
        self.set_status(200)


def make_app():
    return tornado.web.Application([
        (r"/login_pre", PreLoginHandler, dict(user_info=reading_users("admin"))),
        (r"/login_warning", WarningLoginHandler, dict(user_info=reading_users("admin"))),
        (r"/login_man", ManLoginHandler,
         dict(user_info={"Admin": reading_man_users("admin"), "User": reading_man_users("users")})),
        (r"/login_auto", AutoLoginHandler,
         dict(user_info={"Admin": reading_users("admin"), "User": reading_users("users")})),
        (r"/eculist", ECUListHandler),
        (r"/checkhw", HWCheckHandler),
        (r"/hw_disconnected", EventHandler),
        (r"/manedit", ManEditHandler),
        (r"/replace", ReplaceHandler),
        (r"/logout", LogoutHandler),
        (r"/config", ConfigHandler),
        (r"/expiration", ExpirationHandler)
    ])


def reading_users(type_user):
    with open('{}.csv'.format(type_user), 'r') as f:
        reader = csv.DictReader(f, fieldnames=["Name", "Surname", "username", "password", "minimizationTime"],
                                delimiter=',')
        reg = list(reader)
        reg.pop(0)
        f.close()
    return reg


def reading_man_users(type_user):
    with open('{}.csv'.format(type_user), 'r') as f:
        reader = csv.DictReader(f, fieldnames=["Name", "Surname", "username", "password"], delimiter=',')
        reg = list(reader)
        reg.pop(0)
        f.close()
    return reg


def getting_hw(type_hw):
    with open(type_hw + ".csv", "r") as hw_file:
        reader = csv.reader(hw_file)
        my_list = list(reader)
        my_list.pop(0)
        hws = {"id": [], "name": [], "keyCode": [], "note": [], "location": [], "offPeriod": []}
        for r in my_list:
            hws["id"].append(r[0])
            hws["name"].append(r[1])
            hws["keyCode"].append(r[2])
            hws["note"].append(None if (r[3] == "None" or r[3] == "null") else r[3])
            hws["location"].append(dict(loc.split(":") for loc in r[4].split(",")))
            hws["offPeriod"].append(None if (r[5] == "None" or r[3] == "null") else r[5])
        return hws


def hw_check(hw_linked, hw_list):
    for code in hw_list["keyCode"]:
        if hw_linked == code:
            return hw_list["name"][hw_list["keyCode"].index(code)]
    return "Not Found"


def logging(events):
    f = open("events.txt", "a")
    f.write(events)
    f.write('\n')
    f.close()


def read_man_reservation():
    with open('manual reservation.csv', 'r') as f:
        reader = csv.DictReader(f,
                                fieldnames=["id", "name", "startTime", "endTime", "ecu", "sw", "version", "ecuInstance",
                                            "wh", "lp", "project", "activity", "checkIntervalTime", "expirationTime",
                                            "startLunchTime", "endLunchTime"], delimiter=',')
        reg = list(reader)
        reg.pop(0)
        f.close()
    return reg


def read_auto_reservation():
    with open('automatic reservation.csv', 'r') as f:
        reader = csv.DictReader(f, fieldnames=["id", "startTime", "endTime", "wh", "lp"], delimiter=',')
        reg = list(reader)
        reg.pop(0)
        f.close()
    return reg


app = make_app()
app.listen(8888)
ioloop.IOLoop.current().start()
